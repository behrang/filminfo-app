
 == Instalation =======================

-- create virtual host with these general information with your own app path and url

	<VirtualHost *:80>
	    ServerAdmin root@localhost.com
	    DocumentRoot "E:\wamp\www\filminfo-app\public"
	    ServerName filminfo-app.local
	    ServerAlias www.filminfo-app.local
	    SetEnv APPLICATION_ENV "development"
	    <directory "E:\wamp\www\filminfo-app\public">
	        DirectoryIndex index.php
	        AllowOverride all
	        Order Allow,Deny
	        Allow from all
	    </directory>
	</VirtualHost>
	
--	clone project via git repository
	git clone https://gitlab.com/behrang/filminfo-app.git
	or
	git clone git@gitlab.com:behrang/filminfo-app.git

--	setup environment and config file
	create database
	rename or copy .env.local to .env and change database connection information in it
--	use these command line in the root of project to setup

	composer update				// get repository dependency
	php artisan key:generate	// generate app key
	php artisan migrate			// create database table
	php artisan db:seed			// db seeding
	
== How to use api =======================	

Get all films
=============
	URL : project-url/ahttp:/filminfo-app.local/api/v1/films
	Method: get
	return: all film json data

Get all films
=============
	URL : project-url/api/v1/films/{film id}
	Method: get
	return: all film json data
	-- 	Example:
			project-url/api/v1/films/2

Insert new film
===============
	URL : project-url/api/v1/films
	Method: post
	using: form-data
	Return: inserted json data
	
	--	sending data
		name:'text'
		description:'text'
		realease_at:'date format'
		rating:'integer from 1 to 5 integ'
		ticket_price:'price with 2 decimal'
		country:'string'
		photo:'type must be file'
		genre:'string seperated by comma'
	
	-- 	Example:
			project-url/api/v1/films
		
			name:Inception
			description:A thief, who steals corporate secrets
			realease_at:2017-02-05
			rating:5
			ticket_price:55
			country:USA
			photo: file upload
			genre:Music,Drama,Action

Edit film by id
===============
	URL : project-url/api/v1/films/{film id}
	Method: put
	using: x-www-form-urlencoded
	Return: updated json data
	
	-- 	Example:
			project-url/api/v1/films/1
		
			sendign data just like insert film
			
Delete film by id
===============
	URL : project-url/api/v1/films/{film id}
	Method: delete
	Return: true|false
	
	-- 	Example:
			project-url/api/v1/films/2
